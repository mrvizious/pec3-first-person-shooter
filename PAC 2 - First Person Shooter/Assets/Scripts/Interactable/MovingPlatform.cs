﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{

    private Dictionary<GameObject, Transform> previousParents;

    private void Start()
    {
        previousParents = new Dictionary<GameObject, Transform>();
    }


    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.tag.Equals("Player")) AttachGameObject(other.gameObject);
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.collider.tag.Equals("Player")) DetachGameObject(other.gameObject);
    }

    /// <summary>
    /// Attaches the GameObject to the transform and keeps track of its previous parent
    /// </summary>
    /// <param name="gameobjectToAttach">GameObject to detach</param>
    public void AttachGameObject(GameObject gameobjectToAttach)
    {

        if (!previousParents.ContainsKey(gameobjectToAttach))
        {
            Debug.Log("Dictionary size: " + previousParents.Count);
            previousParents.Add(gameobjectToAttach, gameobjectToAttach.transform.parent);
            gameobjectToAttach.transform.SetParent(transform);
            Debug.Log("Object attached: " + gameobjectToAttach, gameobjectToAttach);
        }
    }

    /// <summary>
    /// Detaches the GameObject from the transform only if it was previously attached
    /// </summary>
    /// <param name="gameobjectToDetach">GameObject to detach</param>
    public void DetachGameObject(GameObject gameobjectToDetach)
    {
        if (previousParents.ContainsKey(gameobjectToDetach))
        {
            gameobjectToDetach.transform.SetParent(previousParents[gameobjectToDetach]);
            previousParents.Remove(gameobjectToDetach);
            Debug.LogError("Detached!");
        }
        else Debug.LogError("Object that wants to be detached wasn't previously attached!");
    }
}