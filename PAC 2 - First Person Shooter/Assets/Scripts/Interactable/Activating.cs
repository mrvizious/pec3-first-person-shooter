﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Activating : MonoBehaviour
{
    public virtual void Activate()
    {
        Debug.Log("Activate not implemented!");
    }
}
