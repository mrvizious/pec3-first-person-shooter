﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeMovingObject : Activating
{
    public bool automatic = false;
    public float speed = 0.8f;
    public List<Transform> nodes;
    public bool directionAscending = true;
    private int currentTargetIndex = 0;
    private void Update()
    {
        if (automatic && HasReachedTarget()) SetNextTarget();
        transform.position = Vector3.MoveTowards(transform.position, nodes[currentTargetIndex].position, speed * Time.deltaTime);
    }

    public override void Activate()
    {
        if (automatic) return;
        if (HasReachedTarget()) directionAscending = !directionAscending;
        SetNextTarget();
    }

    private bool HasReachedTarget()
    {
        return Vector3.Distance(transform.position, nodes[currentTargetIndex].position) <= Mathf.Epsilon;
    }
    private void SetClosestNodeAsTarget()
    {
        float minDistance = Mathf.Infinity;
        int closestNode = 0;
        for (int i = 0; i < nodes.Count; i++)
        {
            if (Vector3.Distance(transform.position, nodes[i].position) < minDistance)
            {
                minDistance = Vector3.Distance(transform.position, nodes[i].position);
                closestNode = i;
            }
        }
        currentTargetIndex = closestNode;
    }

    private void SetNextTarget()
    {
        if (currentTargetIndex + (directionAscending ? 1 : -1) >= nodes.Count)
        {
            currentTargetIndex = 0;
        }
        else if (currentTargetIndex + (directionAscending ? 1 : -1) < 0)
        {
            currentTargetIndex = nodes.Count - 1;
        }
        else currentTargetIndex = currentTargetIndex + (directionAscending ? 1 : -1);
    }
}
