﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine;
using UnityEngine.Events;

public abstract class Interactable : MonoBehaviour
{
    public UnityEvent onInteract;
    public virtual void Interact()
    {
        Debug.Log("Interaction not implemented!");
    }
}
