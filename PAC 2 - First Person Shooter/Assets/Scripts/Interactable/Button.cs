﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Button : Interactable
{
    public override void Interact()
    {
        onInteract.Invoke();
    }
}
