﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public float rotationSpeed = 100f;
    public enum CollectibleType
    {
        Ammo,
        Medkit,
        Shield
    }
    public CollectibleType collectibleType;

    private void Update()
    {
        transform.eulerAngles += Vector3.up * rotationSpeed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            switch (collectibleType)
            {
                case CollectibleType.Ammo:
                    other.GetComponent<PlayerWeaponController>().AddBullets();
                    break;
                case CollectibleType.Medkit:
                    other.GetComponent<PlayerHealth>().Heal(50f);
                    break;
                case CollectibleType.Shield:
                    other.GetComponent<PlayerHealth>().AddShield(50f);
                    break;
            }
            Destroy(gameObject);
        }
    }
}
