﻿using UnityEngine;

public class AgentStateMachine : MonoBehaviour
{
    public AgentState firstState, previousState, currentState;

    private void Start()
    {
        firstState = gameObject.GetComponent<AgentState>();
        SetState(firstState);
    }

    public void SetState(AgentState newState)
    {
        if (currentState != null)
        {
            if (previousState != null && previousState != firstState)
            {
                Destroy(previousState);
            }
            previousState = currentState;
            previousState.enabled = false;
        }
        currentState = newState;
        currentState.enabled = true;
        currentState.Begin(this);
    }

    public void GoToPreviousState()
    {
        AgentState temp = currentState;
        currentState = previousState;
        previousState = temp;
        previousState.enabled = false;
        currentState.enabled = true;
        currentState.Begin(this);
    }

    public void GoToFirstState()
    {
        SetState(firstState);
    }
}