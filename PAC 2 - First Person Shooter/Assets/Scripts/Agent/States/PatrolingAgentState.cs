﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolingAgentState : AgentState
{
    public List<Transform> nodes;
    private int currentTarget;

    public override void Begin(AgentStateMachine newAgentStateMachine)
    {
        base.Begin(newAgentStateMachine);
        agent.speed = 3.5f;
        actions.Walk();
        SetClosestNodeAsTarget();
    }

    private void Update()
    {
        if (!SensesPlayer())
        {
            if (HasReachedTarget())
            {
                SetNextTarget();
            }
        }
    }
    private void SetClosestNodeAsTarget()
    {
        float minDistance = Mathf.Infinity;
        int closestNode = 0;
        for (int i = 0; i < nodes.Count; i++)
        {
            if (Vector3.Distance(transform.position, nodes[i].position) < minDistance)
            {
                minDistance = Vector3.Distance(transform.position, nodes[i].position);
                closestNode = i;
            }
        }
        currentTarget = closestNode;

        SetTarget(nodes[currentTarget].position);
    }

    private void SetNextTarget()
    {
        if (currentTarget + 1 == nodes.Count)
        {
            currentTarget = 0;
        }
        else
        {
            currentTarget++;
        }
        SetTarget(nodes[currentTarget].position);

    }

}
