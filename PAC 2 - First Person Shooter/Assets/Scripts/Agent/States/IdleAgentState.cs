﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IdleAgentState : AgentState
{
    private Vector3 initialPosition;
    private void Start()
    {
        setInitialPosition(transform.position);
    }
    public override void Begin(AgentStateMachine newAgentStateMachine)
    {
        base.Begin(newAgentStateMachine);
        SetTarget(initialPosition);
        agent.speed = 3.5f;
        actions.Walk();
    }

    public void setInitialPosition(Vector3 newInitialPosition)
    {
        initialPosition = newInitialPosition;
    }

    private void Update()
    {
        if (!SensesPlayer())
        {
            if (HasReachedTarget()) actions.Stay();
        }
    }
}
