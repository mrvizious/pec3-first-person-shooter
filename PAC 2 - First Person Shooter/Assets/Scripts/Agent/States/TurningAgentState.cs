﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurningAgentState : AgentState
{
    public float turningSpeed = 150f;
    public float degreesRotated = 0f;
    public override void Begin(AgentStateMachine newAgentStateMachine)
    {
        base.Begin(newAgentStateMachine);
        actions.Aiming();
        degreesRotated = 0f;
    }
    void FixedUpdate()
    {
        Transform playerTransform = FindPlayerForward();
        if (playerTransform != null)
        {
            SetTarget(transform.position);
            ShootingAgentState shootingState = gameObject.AddComponent<ShootingAgentState>();
            shootingState.setLastSeenPlayerPosition(playerTransform.position);
            agentStateMachine.SetState(shootingState);
        }
        else
        {

            if (degreesRotated >= 360f || degreesRotated <= -360f)
            {
                Debug.Log("Round completed and no player was found!", this);
                agentStateMachine.GoToFirstState();
            }
            else
            {
                float addition = turningSpeed * Time.fixedDeltaTime;
                degreesRotated += addition;
                transform.eulerAngles = transform.eulerAngles + Vector3.up * addition;
            }
        }
    }
}
