﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadAgentState : AgentState
{
    public override void Begin(AgentStateMachine newAgentStateMachine)
    {
        base.Begin(newAgentStateMachine);
        actions.Death();
        GetComponent<Collider>().enabled = false;
        agent.enabled = false;
        dropObjects = agentStateMachine.firstState.dropObjects;
        Drop();
    }

    private void Drop()
    {
        Debug.Log("Dropping!");
        Debug.Log("Size of dropObjects " + dropObjects.Count);
        GameObject dropObject = dropObjects[Random.Range(0, dropObjects.Count)];
        Instantiate(dropObject, transform.position, transform.rotation);
    }
}
