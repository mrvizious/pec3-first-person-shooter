﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public abstract class AgentState : MonoBehaviour
{
    public List<GameObject> dropObjects;

    protected AgentStateMachine agentStateMachine;
    protected AgentWeaponController weaponController;
    protected NavMeshAgent agent;
    protected Actions actions;
    protected float sphereRadius = 15f;

    public virtual void Begin(AgentStateMachine newAgentStateMachine)
    {
        weaponController = GetComponent<AgentWeaponController>();
        actions = GetComponent<Actions>();
        agent = GetComponent<NavMeshAgent>();
        agentStateMachine = newAgentStateMachine;
        Debug.Log("Agent began! " + this.GetType());
    }

    protected bool IsPlayerNearby()
    {
        bool returnValue = Physics.CheckSphere(transform.position, sphereRadius, 1 << LayerMask.NameToLayer("Player"));
        return returnValue;
    }

    protected Transform FindPlayerForward()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, weaponController.currentWeapon.shotDistance))
        {
            Debug.DrawRay(transform.position + Vector3.up * 1.6f, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
            if (hit.collider.tag.Equals("Player"))
            {
                return hit.collider.transform;
            }
            else return null;
        }
        else return null;
    }

    protected bool SensesPlayer()
    {
        Transform playerTransform = FindPlayerForward();
        if (playerTransform != null)
        {
            SetTarget(transform.position);
            ShootingAgentState shootingState = gameObject.AddComponent<ShootingAgentState>();
            shootingState.setLastSeenPlayerPosition(playerTransform.position);
            agentStateMachine.SetState(shootingState);
            return true;
        }
        else if (IsPlayerNearby())
        {
            SetTarget(transform.position);
            agentStateMachine.SetState(gameObject.AddComponent<TurningAgentState>());
            return true;
        }
        return false;
    }
    public void SetTarget(Vector3 target)
    {
        agent = GetComponent<NavMeshAgent>();
        agent.destination = target;
    }

    protected bool HasReachedTarget()
    {

        if (!agent.pathPending)
        {
            if (agent.remainingDistance <= agent.stoppingDistance)
            {
                if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                {
                    return true;
                }
            }
        }
        return false;
    }

}