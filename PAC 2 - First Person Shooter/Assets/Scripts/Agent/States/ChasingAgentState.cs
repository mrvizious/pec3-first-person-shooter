﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChasingAgentState : AgentState
{
    public override void Begin(AgentStateMachine newAgentStateMachine)
    {
        base.Begin(newAgentStateMachine);
        agent.speed = 7f;
        actions.Run();
    }


    private void Update()
    {
        if (!SensesPlayer())
        {
            if (HasReachedTarget()) agentStateMachine.SetState(gameObject.AddComponent<TurningAgentState>());
        }
    }
}
