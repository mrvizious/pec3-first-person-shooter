﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingAgentState : AgentState
{
    Vector3 lastKnownPlayerPosition;
    public override void Begin(AgentStateMachine newAgentStateMachine)
    {
        base.Begin(newAgentStateMachine);
        actions.Aiming();
    }

    private void Update()
    {
        Transform playerPosition = FindPlayerForward();
        if (playerPosition != null)
        {
            lastKnownPlayerPosition = playerPosition.position;
            LookAtWithoutRotation(playerPosition);
            weaponController.TryToShoot();
        }
        else
        {
            ChasingAgentState chasingState = gameObject.AddComponent<ChasingAgentState>();
            SetTarget(lastKnownPlayerPosition);
            agentStateMachine.SetState(chasingState);
        }
    }

    public void setLastSeenPlayerPosition(Vector3 newLastKnownTargetPosition)
    {
        lastKnownPlayerPosition = newLastKnownTargetPosition;
    }

    private void LookAtWithoutRotation(Transform target)
    {
        transform.LookAt(new Vector3(target.position.x, transform.position.y, target.position.z));
    }

}
