﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentHealth : MonoBehaviour
{
    public float currentHealth = 0f;
    public HealthData healthData;
    private void Start()
    {
        Reset();
    }
    public void Reset()
    {
        currentHealth = healthData.maxHealth;
    }

    public void Heal(float healingAmount)
    {
        currentHealth = Mathf.Clamp(currentHealth + healingAmount, 0f, healthData.maxHealth);
    }

    public void Damage(float damage)
    {
        if (currentHealth > 0)
        {
            currentHealth = Mathf.Clamp(currentHealth - damage, 0f, healthData.maxHealth);
            if (currentHealth <= 0f) GetComponent<AgentStateMachine>().SetState(gameObject.AddComponent<DeadAgentState>());
            else GetComponent<Actions>().Damage();
        }
    }


}
