﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.FirstPerson;

public class AgentWeaponController : MonoBehaviour
{
    private float lastShotTime;
    public Weapon currentWeapon;
    [SerializeField]
    private LayerMask layersToIgnore;

    private void Start()
    {
        ResetLastShotTime();
        layersToIgnore = ~layersToIgnore;
    }

    public void TryToShoot()
    {
        if ((Time.time - lastShotTime) > currentWeapon.timeBetweenShots)
        {
            Shoot();
        }
    }

    public Collision Shoot()
    {
        ResetLastShotTime();
        GetComponent<Actions>().Attack();
        RaycastHit hit;
        Debug.DrawRay(transform.position + transform.up * 1.6f, transform.forward * currentWeapon.shotDistance, Color.red);
        if (Physics.Raycast(transform.position + transform.up * 1.6f, transform.forward, out hit, currentWeapon.shotDistance/*, layersToIgnore*/))
        {
            if (hit.collider.tag.Equals("Player"))
            {
                hit.collider.gameObject.GetComponent<PlayerHealth>().Damage(currentWeapon.damagePerShot);
            }
        }

        return null;
    }

    private void ResetLastShotTime()
    {
        lastShotTime = Time.time;
    }
}
