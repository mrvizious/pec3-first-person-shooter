﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Shield", menuName = "ScriptableObjects/Shields", order = 1)]
public class HealthData : ScriptableObject
{
    [Range(0f, 1f)]
    public float percentageReceivedByPlayer = 0.1f;
    public float currentShield, maxShield = 300f;
    public float currentHealth, maxHealth = 100f;


}
