﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "ScriptableObjects/Weapons", order = 1)]
public class Weapon : ScriptableObject
{
    public GameObject model;
    public bool automatic = false;
    public float timeBetweenShots = 0.2f, recoilMagnitude = 70f;
    public float damagePerShot = 80f;
    public float shotDistance = 30f;
    public int magazineSize = 30, bulletsInMagazine = 0, extraBullets = 0, initialExtraBullets = 60;

}
