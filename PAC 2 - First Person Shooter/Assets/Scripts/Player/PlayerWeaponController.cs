﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerWeaponController : MonoBehaviour
{
    public Text bulletsText;
    private float lastShotTime;
    private InputController input;
    public List<Weapon> weapons;
    public Weapon currentWeapon;
    public int currentWeaponIndex = 0;
    private Transform cameraTransform;
    [SerializeField]
    private LayerMask layersToIgnore;
    private GameObject gunModel;
    private void Start()
    {
        input = GetComponent<InputController>();
        cameraTransform = GetComponentInChildren<Camera>().transform;
        layersToIgnore = ~layersToIgnore;
        ChangeWeapon(currentWeaponIndex);
        ResetLastShotTime();
        StartWeapons();
        UpdateBulletsText();
    }

    private void Update()
    {
        TryToShoot();
        if (input.mouseWheelChange > 0) // forward
        {
            NextWeapon();
        }
        else if (input.mouseWheelChange < 0) // backwards
        {
            PreviousWeapon();
        }

        if (input.reloadButtonJustPressed) Reload();
    }

    private void StartWeapons()
    {
        foreach (Weapon weapon in weapons)
        {
            weapon.extraBullets = weapon.initialExtraBullets;
            weapon.bulletsInMagazine = weapon.magazineSize;
        }
    }

    private void TryToShoot()
    {
        if ((Time.time - lastShotTime) > currentWeapon.timeBetweenShots && currentWeapon.bulletsInMagazine > 0)
        {
            if (currentWeapon.automatic)
            {
                if (input.shootButtonHeldDown) Shoot();
            }
            else
            {
                if (input.shootButtonJustPressed) Shoot();
            }
        }
    }

    public Collision Shoot()
    {
        ResetLastShotTime();

        RaycastHit hit;
        if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, Mathf.Infinity, layersToIgnore))
        {
            if (hit.collider.tag.Equals("Enemy"))
            {
                hit.collider.gameObject.GetComponent<AgentHealth>().Damage(currentWeapon.damagePerShot);
            }
            Debug.DrawRay(cameraTransform.position, cameraTransform.forward * hit.distance, Color.yellow);
        }

        Recoil(currentWeapon.recoilMagnitude);
        currentWeapon.bulletsInMagazine--;

        UpdateBulletsText();

        return null;
    }

    public void Reload()
    {
        if (currentWeapon.bulletsInMagazine >= currentWeapon.magazineSize) return;
        int bulletsToFillMagazine = currentWeapon.magazineSize - currentWeapon.bulletsInMagazine;
        if (currentWeapon.extraBullets - bulletsToFillMagazine < 0)
        {
            currentWeapon.bulletsInMagazine += currentWeapon.extraBullets;
            currentWeapon.extraBullets = 0;
        }
        else
        {
            currentWeapon.bulletsInMagazine += bulletsToFillMagazine;
            currentWeapon.extraBullets -= bulletsToFillMagazine;
        }
        UpdateBulletsText();
    }

    private void Recoil(float recoilMagnitude)
    {
        GetComponent<RigidbodyFirstPersonController>().mouseLook.m_CameraTargetRot.eulerAngles -= Vector3.right * recoilMagnitude * 0.5f;
        cameraTransform.eulerAngles = cameraTransform.eulerAngles - Vector3.right * recoilMagnitude;
    }

    private void ResetLastShotTime()
    {
        lastShotTime = Time.time;
    }

    private void NextWeapon()
    {
        int index = currentWeaponIndex + 1;
        if (index >= weapons.Count) index = 0;
        Debug.Log("New index is " + index);
        ChangeWeapon(index);
    }
    private void PreviousWeapon()
    {
        int index = currentWeaponIndex - 1;
        if (index < 0) index = weapons.Count - 1;
        ChangeWeapon(index);
    }
    private void ChangeWeapon(int index)
    {
        if (index >= 0 || index < weapons.Count)
        {
            currentWeaponIndex = index;
            SetCurrentWeapon(weapons[index]);
        }
    }

    private void SetCurrentWeapon(Weapon newWeapon)
    {
        Destroy(gunModel);
        gunModel = Instantiate(newWeapon.model, transform);
        currentWeapon = newWeapon;
        UpdateBulletsText();
    }

    private void UpdateBulletsText()
    {
        bulletsText.text = currentWeapon.bulletsInMagazine + "/" + currentWeapon.extraBullets;
    }

    public void AddBullets()
    {
        AddBullets(currentWeapon.magazineSize);
    }
    public void AddBullets(int bulletsToAdd)
    {
        currentWeapon.extraBullets += bulletsToAdd;
        UpdateBulletsText();
    }
}
