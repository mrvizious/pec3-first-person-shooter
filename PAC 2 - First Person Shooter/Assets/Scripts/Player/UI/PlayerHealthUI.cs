﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthUI : MonoBehaviour
{
    public Healthbar healthBar;
    public Healthbar shieldBar;

    public HealthData playerHealth;

    private void Update()
    {
        healthBar.maximumHealth = playerHealth.maxHealth;
        healthBar.healthbarDisplay.maxValue = playerHealth.maxHealth;
        healthBar.healthbarDisplay.UpdateVisuals();

        shieldBar.maximumHealth = playerHealth.maxShield;
        shieldBar.healthbarDisplay.maxValue = playerHealth.maxShield;
        shieldBar.healthbarDisplay.UpdateVisuals();

        healthBar.SetHealth(playerHealth.currentHealth);
        shieldBar.SetHealth(playerHealth.currentShield);
    }


}
