﻿using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    public float interactionDistance = 1f;
    public LayerMask layersToIgnore;
    private Transform cameraTransform;
    private InputController input;
    private void Start()
    {
        input = GetComponent<InputController>();
        layersToIgnore = ~layersToIgnore;
        cameraTransform = GetComponentInChildren<Camera>().transform;
    }

    private void Update()
    {
        if (input.actionButtonJustPressed) Interact();
    }

    private void Interact()
    {
        RaycastHit hit;
        if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out hit, interactionDistance, layersToIgnore))
        {
            if (hit.collider.tag.Equals("Interactable"))
            {
                Debug.Log("Interacted!");
                hit.collider.gameObject.GetComponent<Interactable>().Interact();
            }
            Debug.DrawRay(cameraTransform.position, cameraTransform.forward * hit.distance, Color.blue);
            Debug.Log("Hit element with tag: " + hit.collider.tag);
        }
    }
}
