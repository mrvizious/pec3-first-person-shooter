﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public HealthData healthData;
    private void Start()
    {
        Reset();
    }
    public void Reset()
    {
        ResetHealth();
        ResetShield();
    }
    public void ResetShield()
    {
        healthData.currentShield = healthData.maxShield;
    }

    public void ResetHealth()
    {
        healthData.currentHealth = healthData.maxHealth;
    }

    public void Heal(float healingAmount)
    {
        healthData.currentHealth = Mathf.Clamp(healthData.currentHealth + healingAmount, 0f, healthData.maxHealth);
    }

    public void Damage(float totalDamage)
    {
        float damageTaken;
        if (healthData.percentageReceivedByPlayer == 1f || healthData.currentShield <= 0f)
        {
            healthData.currentHealth -= totalDamage;
            damageTaken = totalDamage;
        }
        else
        {
            float playerDamage = totalDamage * healthData.percentageReceivedByPlayer;
            float shieldDamage = totalDamage * (1f - healthData.percentageReceivedByPlayer);
            damageTaken = DamageShieldAndGetExtraDamage(shieldDamage) + playerDamage;
            healthData.currentHealth -= damageTaken;
        }

        if (healthData.currentHealth <= 0f)
        {
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene("DeathMenu");
        }
    }

    public void AddShield(float shieldAmount)
    {
        healthData.currentShield = Mathf.Clamp(healthData.currentShield + shieldAmount, 0f, healthData.maxHealth);
    }

    public float DamageShieldAndGetExtraDamage(float shieldDamage)
    {
        float newShieldHealth = healthData.currentShield - shieldDamage;
        if (newShieldHealth >= 0)
        {
            healthData.currentShield = newShieldHealth;
            return 0f;
        }
        else
        {
            healthData.currentShield = 0f;
            return -newShieldHealth;
        }
    }


}
