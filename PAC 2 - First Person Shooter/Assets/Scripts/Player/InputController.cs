﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public string shootButton = "Fire1", actionButton = "Use", reloadButton = "Reload";

    public bool shootButtonJustPressed = false, shootButtonHeldDown = false, actionButtonJustPressed = false, reloadButtonJustPressed = false;

    public int mouseWheelChange = 0;
    void Update()
    {
        shootButtonJustPressed = Input.GetButtonDown(shootButton);
        shootButtonHeldDown = Input.GetButton(shootButton);
        actionButtonJustPressed = Input.GetButtonDown(actionButton);
        reloadButtonJustPressed = Input.GetButtonDown(reloadButton);
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) mouseWheelChange = 1;
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) mouseWheelChange = -1;
        else mouseWheelChange = 0;
    }
}
