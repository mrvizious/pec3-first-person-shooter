﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CharacterPanel : MonoBehaviour
{

    public GameObject character;
    public Transform weaponsPanel;
    public Transform actionsPanel;
    public Transform camerasPanel;
    public UnityEngine.UI.Button buttonPrefab;
    public Slider motionSpeed;

    Actions actions;
    CharacterController controller;
    Camera[] cameras;

    void Start()
    {
        Initialize();
    }

    void Initialize()
    {
        actions = character.GetComponent<Actions>();
        controller = character.GetComponent<CharacterController>();

        foreach (CharacterController.Arsenal a in controller.arsenal)
            CreateWeaponButton(a.name);

        CreateActionButton("Stay");
        CreateActionButton("Walk");
        CreateActionButton("Run");
        CreateActionButton("Sitting");
        CreateActionButton("Jump");
        CreateActionButton("Aiming");
        CreateActionButton("Attack");
        CreateActionButton("Damage");
        CreateActionButton("Death Reset", "Death");

        cameras = GameObject.FindObjectsOfType<Camera>();
        var sort = from s in cameras orderby s.name select s;

        foreach (Camera c in sort)
            CreateCameraButton(c);

        camerasPanel.GetChild(0).GetComponent<UnityEngine.UI.Button>().onClick.Invoke();
    }

    void CreateWeaponButton(string name)
    {
        UnityEngine.UI.Button button = CreateButton(name, weaponsPanel);
        button.onClick.AddListener(() => controller.SetArsenal(name));
    }

    void CreateActionButton(string name)
    {
        CreateActionButton(name, name);
    }

    void CreateActionButton(string name, string message)
    {
        UnityEngine.UI.Button button = CreateButton(name, actionsPanel);
        button.onClick.AddListener(() => actions.SendMessage(message, SendMessageOptions.DontRequireReceiver));
    }

    void CreateCameraButton(Camera c)
    {
        UnityEngine.UI.Button button = CreateButton(c.name, camerasPanel);
        button.onClick.AddListener(() =>
        {
            ShowCamera(c);
        });
    }

    UnityEngine.UI.Button CreateButton(string name, Transform group)
    {
        GameObject obj = (GameObject)Instantiate(buttonPrefab.gameObject);
        obj.name = name;
        obj.transform.SetParent(group);
        obj.transform.localScale = Vector3.one;
        Text text = obj.transform.GetChild(0).GetComponent<Text>();
        text.text = name;
        return obj.GetComponent<UnityEngine.UI.Button>();
    }

    void ShowCamera(Camera cam)
    {
        foreach (Camera c in cameras)
            c.gameObject.SetActive(c == cam);
    }

    void Update()
    {
        Time.timeScale = motionSpeed.value;
    }

    public void OpenPublisherPage()
    {
        Application.OpenURL("https://www.assetstore.unity3d.com/en/#!/publisher/11008");
    }
}
