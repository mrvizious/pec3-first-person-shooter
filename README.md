# PEC 2: First Person Shooter

Este juego ha sido realizado por Javier Riera, y dispone de dos niveles:

## Nivel Montañoso

En este nivel tienes que infiltrarte en un campamento militar que oculta la entrada a un búnker, camuflándola como un contenedor cerrado. Por suerte, te han avisado de que la llave para entrar es pequeña y blanca, y que debe estar por el propio campamento. También te han avisado de que hay una manera de colarse si das una vuelta alrededor de la verja.

**Tutorial**: Da una vuelta hacia la izquierda de la verja, hasta que encuentres un elevador. Móntate en el palé que tiene y activa el aparato pulsando la tecla E sobre el vehículo. Mata a los dos guardias que están junto a la mesa de póker y, posiblemente, al guardia que pasea por dentro, si es que está cerca.

Acércate a la mesa de póker y observa que hay una ficha blanca en el suelo. Mírala y pulsa la E para recogerla: esto es el chip. Ahora puedes acercarte a la puerta de contenedor que está en el centro del campamento, iluminado por una farola. Ahora que tienes el chip, puedes pulsar la tecla E mientras miras al escáner que está en la puerta derecha para que la puerta izquierda se abra.

Pasarás así al segundo nivel.

## Nivel en Interior

**Tutorial**: Una vez hemos entrado en el búnker, tenemos que recuperar los códigos nucleares que se hayan en un ordenador protegido por tres puertas. Avanza por los diferentes pisos, elimina a los enemigos y pulsa E una vez sobre los ordenadores de colores que hay repartidos, de esta manera se abrirán las puertas de la habitación donde está el ordenador al que tienes que llegar.

# Estructuras de código

## Armas

Para facilitar la creación rápida de armas, se guardan sus datos en ScriptableObjects, de tal manera que podemos especificar su cadencia, daño, alcance, retroceso, tamaño de cargador y balas iniciales extra. También se guarda un GameObject con el modelo del arma en la posición adecuada para el jugador.

El control de estas armas se realiza por medio de un controlador para el jugador y uno diferente para los agentes, pero ambos usan el mismo tipo de ScriptableObjects, de tal manera que se podría cambiar rapidamente y darle a un enemigo un arma normalmente controlada por el jugador, como un revólver.

Además, las armas pueden ser automáticas o no, haciendo que el jugador tenga que pulsar varias veces el botón de disparo para disparar o que lo pueda mantener pulsado.

Se puede cambiar de arma actual con la rueda del ratón, y se recarga con la R.

## Input

Para manejar el input del jugador y poder pararlo en algún momento, se usa un script del cual beben todos los scripts propios creados para el funcionamiento del juego.

Este script actualiza cada frame ua serie de variables, como si se acaba de disparar o se está manteniendo el botón de disparo, si se acaba de saltar o si se acaba de pulsar el botón de interactuar. También guarda la información de si se ha movido la rueda del ratón y hacia dónde.

## Agentes

Los agentes disponen de un sistema de estados que les ayuda a moverse y ejecutar patrones.

Para empezar, tienen 2 posibles estados iniciales, a los cuales volverán en algunas ocasiones si pierden la pista del jugador. Estos dos estados pueden ser Idle o Patroling, que son, respectivamente, uno en el que el agente está quieto en el sitio mirando al frente, y uno en el que pasea entre una serie de puntos otorgados como camino.

De este estado puede pasar a un estado de giro si se acerca mucho el jugador, dado que simula haberle oído. En este estado, el personaje dará una vuelta entera y, si no encuentra al jugador en ningún momento, volverá al estado inicial. Si en cambio encuentra al personaje, empezará inmediatamente a dispararle, entrando en un estado de disparo. A este estado también se puede llegar desde cualquier estado inicial si el jugador se pone directamente en el punto de mira del agente.

Si el jugador sale del rango de disparo del agente, éste correrá hasta el último punto donde le vio, entrando así en el estado de Chasing. Si ve o escucha al jugador, pasará respectivamente al estado de Turning o de Shooting. Si en cambio no le siente, entrará en un estado del que no puede volver, llamado Dead, en el que arrojará un coleccionable aleatorio entre munición, escudo o kits médicos.

## Vida y escudo

El jugador tiene un ScriptableObject en el que se guarda la información de vida máxima, vida actual, escudo máximo y escudo actual. Si se le hace daño y tiene escudo, un porcentaje (configurable en el ScriptableObject) del daño se lo llevará el personaje y el resto el escudo. Si el daño destruye el escudo y queda daño por hacerse, este daño lo recibirá entero el jugador.

El control de estas variables lo tiene un script propio de salud del jugador, dado que estos mismos datos los usan los agentes con su propia instancia del ScriptableObject, que además tienen su propio script de control de la vida simplificado y que almacena la vida en local, porque si no dañar a un solo enemigo dañaría a todos por la naturaleza de los ScriptableObjects.

## Eventos

Cuando se quiere relacionar una acción con una reacción, se utiliza un sistema de objetos derivados de Activating y Interactable. Por ejemplo, para hacer un botón que abra una puerta, como en el segundo nivel, se añade al ordenador (en este caso) un script de Button, que deriva de Interactable. A su vez, a la puerta se le añade un script que deriva de Activating que, al activarse, mueve el objeto de un punto a otro de los especificados, similar a cómo funciona el agente en su estado de Patroling. El ordenador además tendrá un tag Interactable, haciendo que se pueda usar por un script que usa el jugador, que al pulsar la tecla de acción, busca un objeto con el que interactuar donde apunta la mira e intenta interactuar con él.

El Button lanza invoca un evento al ser interactuado, que se puede asignar desde el inspector con cualquier otra función de cualquier otro objeto. De esta manera, desde el inspector se le pone que al interactuar, se llame a la función Activate del script de mover la puerta, haciendo así un mecanismo de acción reacción.

## Movimiento del jugador

Para mover el jugador, se utiliza una versión con parámetros modificados del script que ofrece Unity en sus StandardAssets, en concreto la versión de RigidBody, pue daba menos problemas con las plataformas móviles.

## UI

Las balas se actualizan desde el script que controla las armas, con una referencia al texto que se muestra en el HUD.

La vida utiliza unos prefabs descargados desde la Unity Store, pero utilizados de manera propia para integrarlos en el sistema del resto de scripts. De esta manera, el encargado de cambiar los datos es el mismo que el que se encarga de manejar la vida del jugador.